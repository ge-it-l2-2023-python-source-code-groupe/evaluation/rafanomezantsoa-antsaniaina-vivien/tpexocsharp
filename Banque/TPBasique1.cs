﻿using Banque;

namespace TPBasique1;
static class ExecutionTP1 {
    public static void Executer() {
        Console.WriteLine("\nTP basique 1 : Banque");
        Compte.ReinitialiserNumComptes();

        Console.WriteLine("\nCréation du compte 1 :");
        Console.Write("\nEntrez le nom : ");
        string nom = Console.ReadLine() ?? "";
        Console.Write("Entrez le prénom : ");
        string prenom = Console.ReadLine() ?? "";
        Console.Write("Entrez le CIN : ");
        string cin = Console.ReadLine() ?? "";
        Console.Write("Entrez le numéro de téléphone : ");
        string tel = Console.ReadLine() ?? "";

        Client cl1 = new Client(cin, nom, prenom, tel);
        Compte ccl1 = new Compte(cl1);

        Console.WriteLine("\n******************************\n");

        Console.WriteLine("Compte 1 : ");
        ccl1.AfficherCompte();

        Console.WriteLine("\n******************************\n");
        
        float somme;

        while (true) {
            Console.Write("Créditez le compte : ");
            if (float.TryParse(Console.ReadLine(), out somme)) 
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        ccl1.Crediter(somme);
        Console.WriteLine("\nOpération effectuée.");
        Console.WriteLine($"Le solde du compte 1 est {ccl1.Solde}.");
        Console.ReadKey();

        Console.WriteLine("\n******************************\n");

        Console.WriteLine("Création du compte 2 : ");
        Console.Write("\nEntrez le nom : ");
        nom = Console.ReadLine() ?? "";
        Console.Write("Entrez le prénom : ");
        prenom = Console.ReadLine() ?? "";
        Console.Write("Entrez le CIN : ");
        cin = Console.ReadLine() ?? "";
        Console.Write("Entrez le numéro de téléphone : ");
        tel = Console.ReadLine() ?? "";

        Client cl2 = new Client(cin, nom, prenom, tel);
        Compte ccl2 = new Compte(cl2);

        Console.WriteLine("\n******************************\n");
        
        Console.WriteLine("Compte 2 : ");
        ccl2.AfficherCompte();

        Console.WriteLine("\n******************************\n");
        
        while (true) {
            Console.Write("Créditez le compte : ");
            if (float.TryParse(Console.ReadLine(), out somme)) 
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        ccl2.Crediter(somme);
        Console.WriteLine("\nOpération effectuée.");
        Console.WriteLine($"Le solde du compte 2 est {ccl2.Solde}.");
        Console.ReadKey();

        Console.WriteLine("\n******************************\n");
        
        while (true) {
            Console.Write(@"Créditer le compte 1 à partir du compte 2
Entrez le montant à créditer : ");
            if (float.TryParse(Console.ReadLine(), out somme)) 
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }
        ccl1.Crediter(somme, ccl2);
        Console.WriteLine("\nOpération effectué.");
        Console.ReadKey();

        Console.WriteLine("\n******************************\n");
        
        ccl1.AfficherCompte();
        
        Console.WriteLine("\n******************************\n");
        
        ccl2.AfficherCompte();
        Console.ReadKey();
    }
}
