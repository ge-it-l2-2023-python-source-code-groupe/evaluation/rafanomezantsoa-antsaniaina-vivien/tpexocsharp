namespace Banque;
class Client {
    public string Cin { get; set; }
    public string Nom { get; set; }
    public string Prenom { get; set; }
    public string Tel { get; set; }

    static Client() {

    }

    public Client() {
        Cin = "";
        Nom = "";
        Prenom = "";
        Tel = "";
    }

    public Client(string cinClient, string nomClient, string prenomClient): this() {
        Cin = cinClient;
        Nom = nomClient;
        Prenom = prenomClient;
    }

    public Client(string cinClient, string nomClient, string prenomClient, string telClient):
    this(cinClient,nomClient,prenomClient) {
            Tel = telClient;
    }

    public void Afficher() {
        Console.WriteLine($@"CIN : {Cin} 
Nom : {Nom} 
Prénom : {Prenom} 
Téléphone : {Tel}");
    }
}