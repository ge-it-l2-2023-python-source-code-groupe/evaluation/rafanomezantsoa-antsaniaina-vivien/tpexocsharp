namespace Banque;
class Compte {
    public float Solde { get; private set; }
    public int Code { get; private set; }
    private static int num = 0;
    public Client Cl { get; set; }

    static Compte() {

    }

    public Compte(Client cli) {
        Solde = 0;
        Code = ++num;
        Cl = cli;
    }

    public static void ReinitialiserNumComptes() {
        num = 0;
    }

    public void Crediter(float somme) {
        Solde += somme;
    }

    public void Crediter(float somme, Compte compte) {
        Solde += somme;
        compte.Debiter(somme);
    }

    public void Debiter(float somme) {
        Solde -= somme;
    }

    public void Debiter(float somme, Compte compte) {
        Solde -= somme;
        compte.Crediter(somme);
    }

    public void AfficherCompte() {
        Console.WriteLine($@"Numéro du compte : {Code} 
Solde du compte : {Solde} 
Propriétaire du compte :");
        Cl.Afficher();
    }
}