﻿using TPBasique1;
using TPBasique2;
using TPBasique3;
using TPBasique4;

namespace TpExoCsharp;

class Program {
    static void Main(string[] args) {
        while (true) {
            Console.Clear();
            Console.WriteLine("TP C#");
            Console.Write(@"
Menu principal

Choisissez une option :
1 - TP basique 1 : Banque
2 - TP basique 2 : Cercle
3 - TP basique 3 : Jeu de dés
4 - TP basique 4 : Horloge
5 - Quitter

Quel est votre choix ? ");

            string choix = Console.ReadLine() ?? "";
            Console.Clear();

            if (choix == "1") {
                ExecutionTP1.Executer();
            } else if (choix == "2") {
                ExecutionTP2.Executer();
            } else if (choix == "3") {
                ExecutionTP3.Executer();
            } else if (choix == "4") {
                ExecutionTP4.Executer();
            } else if (choix == "5") {
                break;
            }
        }
    }
}
