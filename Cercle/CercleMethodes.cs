using System.Security.Cryptography.X509Certificates;

namespace CerclePoint;

partial class Cercle {
    public double GetPerimeter() {
        return 2 * Math.PI * Rayon;
    }

    public double GetSurface() {
        return Math.PI * Rayon * Rayon;
    }

    public bool IsInclude(Point p) {
        return Math.Sqrt(Math.Pow(Centre.X - p.X, 2) + Math.Pow(Centre.Y - p.Y, 2)) < Rayon;
    }

    public void Display() {
        Console.WriteLine($"CERCLE({Centre.X}, {Centre.Y}, {Rayon})");
    }
}