namespace CerclePoint;

partial class Point {
    public float X { get; set; }
    public float Y { get; set; }

    public Point() {
        X = default;
        Y = default;
    }
    
    public Point(float xArg) : this() {
        X = xArg;
    }

    public Point(float xArg, float yArg) : this(xArg) {
        Y = yArg;
    }
}