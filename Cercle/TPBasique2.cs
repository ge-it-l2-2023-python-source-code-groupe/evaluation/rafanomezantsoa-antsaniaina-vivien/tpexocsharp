using CerclePoint;

namespace TPBasique2;

static class ExecutionTP2 {
    public static void Executer() {
        Console.WriteLine("\nTP basique 2 : Cercle\n");

        float abs, ord, r, x, y;

        while (true) {
            Console.Write("Donnez l'abscisse du centre : ");
            if (float.TryParse(Console.ReadLine(), out abs)) 
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        while (true) {
            Console.Write("Donnez l'ordonnée du centre : ");
            if (float.TryParse(Console.ReadLine(), out ord))  
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        while (true) {
            Console.Write("Donnez le rayon : ");
            if (float.TryParse(Console.ReadLine(), out r))  
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        Cercle c = new Cercle(abs, ord, r);
        Console.WriteLine("");
        c.Display();

        Console.WriteLine($"Le périmètre est : {c.GetPerimeter():F2}");
        Console.WriteLine($"La surface est : {c.GetSurface():F2}");
        Console.ReadKey();

        Console.WriteLine("\nDonnez un point : ");
        while (true) {
            Console.Write("x : ");
            if (float.TryParse(Console.ReadLine(), out x))  
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        while (true) {
            Console.Write("y : ");
            if (float.TryParse(Console.ReadLine(), out y))  
                break;
            else 
                Console.WriteLine("Entrée invalide");
        }

        Point p = new Point(x, y);
        p.Display();

        Console.WriteLine(c.IsInclude(p) ? 
        "Le point appartient au cercle." : "Le point n'appartient pas au cercle.");
        Console.ReadKey();
    }
}