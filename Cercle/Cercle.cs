namespace CerclePoint;

partial class Cercle {
    public Point Centre { get; set; }
    public float Rayon { get; set; }

    public Cercle() {
        Centre = new Point();
        Rayon = default;
    }

    public Cercle(float x, float y) : this() {
        Centre = new Point(x, y);
    }

    public Cercle(float x, float y, float r) : this(x, y) {
        Rayon = r;
    }
}