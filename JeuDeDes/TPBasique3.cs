﻿using GestionJoueurs;
using MancheJeu;

namespace TPBasique3;

static class ExecutionTP3 {
    public static void Executer() {
        Console.WriteLine("\nJeu de dés\n");
        String listeManches = "";
        Joueurs.listeJoueurs = "";
        Joueurs.scoresJoueurs = "";
        Manche.ReinitialiserNumManche();

        while (true) {
            Console.Write("Entrez le nombre de joueurs : ");
            if (Int32.TryParse(Console.ReadLine(), out Joueurs.nbJoueurs))
                break;
            else
                Console.WriteLine("Entrée invalide !");
        }
        Console.WriteLine("");

        Int32 num = 1;
        while(true) {
            Console.Write($"Entrez le nom du joueur n° {num} : ");
            String nom = Console.ReadLine() ?? "aa";

            Joueurs.AjouterJoueur(nom);

            if(num == Joueurs.nbJoueurs) {
                break;
            }
            num++;
        }
        Console.Clear();

        while(true) {
            Console.WriteLine(@"
Menu principal du Jeu de dés

Scores actuels : ");
            Console.Write(Joueurs.FormaterScore());
            Console.Write($@"
Choisissez une option : 
1 - Nouvelle manche 
2 - Voir l'historique des manches 
3 - Quitter 

Quel est votre choix ? ");

            String choix = Console.ReadLine() ?? "";
            Console.Clear();
            Console.WriteLine("");

            if(choix.Equals("1")) {
                Manche thisManche = new Manche(Joueurs.nbJoueurs, Joueurs.listeJoueurs) {
                    DateHeure = DateTime.Now.ToString()
                };

                num = 1;
                String resultat = "";
                while(true) {
                    while(true) {
                        Console.Write($"Tour de {Joueurs.GetNomJoueur(num-1)} -- Voulez-vous lancer le dé ? (y/n) ");
                        choix = Console.ReadLine() ?? "";
                        if(choix.Equals("y") || choix.Equals("n")) {
                            break;
                        } else {
                            Console.WriteLine("Entrée invalide !");
                        } 
                    }

                    if(choix.Equals("y")) {
                        resultat += Joueurs.Jouer() + ",";
                    } else if(choix.Equals("n")) {
                        resultat += 0 + ",";
                    } 

                    if(num == Joueurs.nbJoueurs) {
                        break;
                    }
                    num++;
                }

                thisManche.SetPointsJoueurs(resultat);
                Joueurs.SetScoresJoueurs(thisManche.PointsJoueurs);
                thisManche.EtatScores = Joueurs.FormaterScore();

                Console.WriteLine(thisManche.InfoManche());
                listeManches += thisManche.InfoManche() + "\n";
                Console.ReadKey();
            } else if(choix.Equals("2")) {
                if(listeManches.Equals(""))
                    Console.WriteLine("L'historique est vide.");
                else
                    Console.WriteLine(listeManches);
                Console.ReadKey();
            } else if(choix.Equals("3")) {
                Console.Clear();
                break;
            }
            Console.Clear();
        }
    }
}
