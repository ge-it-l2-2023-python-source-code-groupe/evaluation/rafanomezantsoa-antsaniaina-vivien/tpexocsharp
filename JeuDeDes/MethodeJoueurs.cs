namespace GestionJoueurs;
static partial class Joueurs {
    public static void AjouterJoueur(String nom) {
        listeJoueurs += nom + ",0;";
    }
    public static String GetNomJoueur(Int32 index) {
        return listeJoueurs.Split(";")[index].Split(",")[0];
    }
    public static String GetScoreJoueur(Int32 index) {
        return listeJoueurs.Split(";")[index].Split(",")[1];
    }
    public static void AjouterScoreJoueur(String nom, Int32 score) {
        String tmpListeJoueurs = "";
        for(Int32 i = 0; i < nbJoueurs; i++) {
            if(GetNomJoueur(i).Equals(nom)) {
                score += Int32.Parse(GetScoreJoueur(i));
                tmpListeJoueurs += GetNomJoueur(i) + "," + score + ";";
            } else {
                tmpListeJoueurs += GetNomJoueur(i) + "," + GetScoreJoueur(i) + ";";
            }
        }
        listeJoueurs = tmpListeJoueurs;
    }
    public static void SetScoresJoueurs(String listePoints) {
        for(Int32 i = 0; i < nbJoueurs; i++) {
            AjouterScoreJoueur(GetNomJoueur(i), Int32.Parse(listePoints.Split(",")[i]));
        }
    }
    public static String FormaterScore() {
        String affichageScore = "";
        for(Int32 i = 0; i < nbJoueurs; i++) {
                affichageScore += "- " + GetNomJoueur(i) + " : " + GetScoreJoueur(i) + " points\n";
        }
        return affichageScore;
    }
    public static Int32 Jouer() {
        Random rnd = new Random();
        Int32 val = rnd.Next(1, 7);

        if(val == 1) {
            Console.WriteLine(deUn);
        } else if(val == 2) {
            Console.WriteLine(deDeux);
        } else if(val == 3) {
            Console.WriteLine(deTrois);
        } else if(val == 4) {
            Console.WriteLine(deQuatre);
        } else if(val == 5) {
            Console.WriteLine(deCinq);
        } else {
            Console.WriteLine(deSix);
        }

        return val;
    }
}