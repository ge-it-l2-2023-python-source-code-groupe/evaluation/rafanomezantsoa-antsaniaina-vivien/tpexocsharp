namespace MancheJeu;
partial class Manche {
    public static void ReinitialiserNumManche() {
        num = 0;
    }
    public String InfoManche() {
        return $@"---------------------------------------
Manche numéro {NumManche}

Date : {DateHeure}

Scores des Joueurs : 
{EtatScores}
Points gagnés par chaque joueur :
{this.FormatterPoints()}";
    }

    public String FormatterPoints() {
        String affichagePoints = "";
        for(Int32 i = 0; i < NbJoueursManche; i++) {
                affichagePoints += $"- {ListeJoueursManche.Split(";")[i].Split(",")[0]} : " 
                + $"{PointsJoueurs.Split(",")[i]} points\n";
        }
        return affichagePoints;
    }

    public void SetPointsJoueurs(String resultat) {
        Int32 max = Int32.Parse(resultat.Split(",")[0]);
        Int32 nbMax = 0;

        for(Int32 i = 1; i < NbJoueursManche; i++) {
            if(Int32.Parse(resultat.Split(",")[i]) > max) {
                max = Int32.Parse(resultat.Split(",")[i]);
            }
        }

        for(Int32 i = 0; i < NbJoueursManche; i++) {
            if(Int32.Parse(resultat.Split(",")[i]) == max) {
                nbMax++;
            }
        }

        for(Int32 i = 0; i < NbJoueursManche; i++) {
            if(max == 6 && Int32.Parse(resultat.Split(",")[i]) == max && nbMax == 1) {
                PointsJoueurs += 2 + ",";
            } else if(max == 6 && Int32.Parse(resultat.Split(",")[i]) == max) {
                PointsJoueurs += 1 + ",";
            } else {
                PointsJoueurs += 0 + ",";
            }
        }
    }
}