namespace GestionJoueurs;
static partial class Joueurs {
    public static Int32 nbJoueurs;
    public static String listeJoueurs = "";
    public static String scoresJoueurs = "";

    private static String deUn = "|       |\n|   *   |\n|       |";
    private static String deDeux = "| *     |\n|       |\n|     * |";
    private static String deTrois = "| *     |\n|   *   |\n|     * |";
    private static String deQuatre = "| *   * |\n|       |\n| *   * |";
    private static String deCinq = "| *   * |\n|   *   |\n| *   * |";
    private static String deSix = "| *   * |\n| *   * |\n| *   * |";

}