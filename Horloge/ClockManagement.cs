using ConsoleManaging;

namespace ClockManaging;

static class ClockManagement {
    public static readonly IList<TimeZoneInfo> TimeZoneList = TimeZoneInfo.GetSystemTimeZones();
    public static readonly string LocalTimeZoneId = TimeZoneInfo.Local.Id;
    public static List<TimeZoneInfo> TZToDisplay = new List<TimeZoneInfo> {};
    private static Task Task1 = Task.Run(() => {});
    private static Task Task2 = Task.Run(() => {});
    private static CancellationTokenSource cts1 = new CancellationTokenSource();
    private static CancellationTokenSource cts2 = new CancellationTokenSource();
    private static List<TimeZoneInfo> suggestions = new List<TimeZoneInfo> {};

    static async void ExecuteTask(Task task) {
        try {
            await task;
        } catch(TaskCanceledException e) {
            e.GetBaseException();
        }
    }

    public static void DisplayHorloge() {
        cts1 = new CancellationTokenSource();

        Task1 = Task.Run(() => {
            while (!cts1.IsCancellationRequested) {
                ConsoleManagement.SetLineAtFromHere(DateTime.Now.ToLongTimeString(), 7);

                Thread.Sleep(1000);
            }
        }, cts1.Token);


        Task1.Wait(1000);
        ExecuteTask(Task1);
    }

    public static void StopDisplayHorloge() {
        cts1.Cancel();
        //cts.Dispose();
    }

    public static void DisplayHorlogesRegions() {
        cts2 = new CancellationTokenSource();

        Task2 = Task.Run(() => {
            foreach (TimeZoneInfo timeZone in TZToDisplay) {
                Console.WriteLine("");
                Console.WriteLine("");
            }
            while (!cts2.IsCancellationRequested) {
                int i = 0;
                foreach (TimeZoneInfo timeZone in TZToDisplay) {
                    ConsoleManagement.SetLineAtFromHere(timeZone.DisplayName, 2*(TZToDisplay.Count - i));
                    ConsoleManagement.SetLineAtFromHere(TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone).ToLongTimeString(), 
                    2*(TZToDisplay.Count - i) - 1);
                    i++;
                    Thread.Sleep(500);
                }                
            }
        }, cts2.Token);

        Task2.Wait(500);
        ExecuteTask(Task2);
    }

    public static void StopDiplayHorlogesRegions() {
        cts2.Cancel();
        //cts2.Dispose();
    }

    public static void SearchClock() {
        while (true) {
            Console.WriteLine("Entrez votre recherche (tapez 2 fois ENTRER pour voir les suggestions ou ENTRER puis ESC pour quitter): ");
            string searchString = Console.ReadLine() ?? "";

            bool exit = false;
            ConsoleKeyInfo esc = Console.ReadKey();
                if (esc.Key == ConsoleKey.Escape)
                        exit = true;
            if (exit)
                break;

            suggestions = SearchSuggestion(searchString);

            if(suggestions.Count > 1) {
                Console.WriteLine("Suggestions : ");
                foreach (TimeZoneInfo suggestion in suggestions)
                {
                    Console.WriteLine(suggestion.DisplayName);
                }
            } else if (suggestions.Count == 0) {
                Console.WriteLine("Aucune suggestion trouvée.");
            } else if (!ClockManagement.TZToDisplay.Contains(suggestions[0])) {
                Console.WriteLine(suggestions[0].DisplayName);
                TZToDisplay.Add(suggestions[0]);
                break;
            } else {
                Console.WriteLine("L'horloge est déjà actif.");
                break;
            }
        }
    }

    public static List<TimeZoneInfo> SearchSuggestion(string searchString) {
        List<TimeZoneInfo> correspondence = new List<TimeZoneInfo> {};
        foreach (TimeZoneInfo element in TimeZoneList) {
            if (element.DisplayName.ToLower().Contains(searchString.ToLower())) {
                correspondence.Add(element);
            }
        }
        return correspondence;
    }
}