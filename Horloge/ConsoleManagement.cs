using ClockManaging;

namespace ConsoleManaging;

static class ConsoleManagement {

    public static void SetLineAtFromHere(string setString, int numOfLine) {
        int CursorLeft = Console.CursorLeft;
        int cursorTop = Console.CursorTop;
        if (cursorTop - numOfLine < 0) cursorTop += - (cursorTop - numOfLine);
        Console.SetCursorPosition(0, cursorTop - numOfLine);

        Console.Write(setString);

        Console.SetCursorPosition(CursorLeft, cursorTop);
    }

}