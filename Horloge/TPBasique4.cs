﻿using System.Linq.Expressions;
using ClockManaging;
using ConsoleManaging;

namespace TPBasique4;

static class ExecutionTP4 {
    public static void Executer() {
        List<Alarm> alarmList = new List<Alarm> {};
        Console.Clear();

        Console.WriteLine("\nTP basique 4 : Horloge");

        while (true) {
            Console.Clear();
            Console.Write(@"
Bienvenue dans le menu principale de l'horloge
Choisissez une option : 
1 - Alarme 
2 - Horloge 
3 - Quitter 

Quel est votre choix ? ");

            string choice = Console.ReadLine() ?? "";

            bool executer = true;

            if (choice == "1") {
                while (executer) {
                    Console.Clear();
                    Console.Write(@"
Choisissez une option : 
1 - Voir les Alarmes actifs
2 - Creer une alarme 
3 - Retour menu principale

Quel est votre choix ? ");

                    string alarmChoice = Console.ReadLine() ?? "";

                    if (alarmChoice == "1") {
                        if (alarmList.Count == 0) {
                            Console.WriteLine("Il n'y a pas d'alarme actif.");
                            Console.ReadKey();
                        } else {
                            foreach(Alarm alarm in alarmList) {
                                Console.WriteLine(alarm.FormatAlarm());
                            }
                            Console.ReadKey();
                        }
                    } else if (alarmChoice == "2") {
                        Console.WriteLine("Infos de la nouvelle alarme :");
                        Console.Write("Nom de l'alarme : ");
                        string alarmName = Console.ReadLine() ?? "Réveil";
                        Console.Write("Heure de l'alarme (hh:mm) : ");
                        bool a = TimeOnly.TryParse(Console.ReadLine(), out TimeOnly alarmTime);
                        string toRepeat = "";
                        while (!toRepeat.ToLower().Equals("o") && !toRepeat.ToLower().Equals("n")) {
                            Console.Write("Répéter l'alarme ? (O/n) ");
                            toRepeat = Console.ReadLine() ?? "n";
                        }
                        bool isRepeted;
                        if (toRepeat.ToLower()[0] == 'o') {
                            isRepeted = true;
                        } else {
                            isRepeted = false;
                        }

                        string alarmDays;

                        if (isRepeted) {
                            Console.Write("Jours de répétition (L/M/ME/J/V/S/D) : ");
                            alarmDays = Console.ReadLine() ?? "L";
                        } else {
                            Console.Write("Jours de l'alarme (L/M/ME/J/V/S/D) : ");
                            alarmDays = Console.ReadLine() ?? "L";
                        }

                        Alarm myAlarm = new Alarm(alarmName, alarmTime, alarmDays, isRepeted);
                        foreach (string day in myAlarm.AlarmDaysString)
                            Console.WriteLine(day);

                        alarmList.Add(myAlarm);
                    } else if (alarmChoice == "3") {
                        executer = false;
                    }
                }
            } else if (choice == "2") {
                while (executer) {
                    Console.Clear();
                    Console.WriteLine($"Heure actuelle ({ClockManagement.LocalTimeZoneId}).\n\n");

                    Console.Write(@"
Choisissez une option : 
1 - Voir les Horloges actifs
2 - Ajouter une horloge 
3 - Retour menu principale

Quel est votre choix ? ");
                    
                    ClockManagement.DisplayHorloge();
                    string clockChoice = Console.ReadLine() ?? "";
                    ClockManagement.StopDisplayHorloge();
                    Thread.Sleep(500);
                    Console.Clear();

                    if (clockChoice == "1") {
                        if (ClockManagement.TZToDisplay.Count > 0) {
                            ClockManagement.DisplayHorlogesRegions();
                            Console.ReadKey();
                            ClockManagement.StopDiplayHorlogesRegions();
                            Thread.Sleep(500);
                        } else {
                            Console.WriteLine("Il n'y a pas d'horloge à afficher.");
                            Console.ReadKey();
                        }
                    } else if (clockChoice == "2") {
                        ClockManagement.SearchClock();
                    } else if (clockChoice == "3") {
                        executer = false;
                    }
                }
            } else if (choice == "3") {
                break;
            }
        }
    }

}
